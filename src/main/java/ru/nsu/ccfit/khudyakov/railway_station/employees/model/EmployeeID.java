package ru.nsu.ccfit.khudyakov.railway_station.employees.model;

import javax.persistence.Column;

public class EmployeeID {
    @Column(name = "employeeID")
    private Integer employeeID;

    public EmployeeID() {

    }

    public Integer getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(Integer employeeID) {
        this.employeeID = employeeID;
    }
}
