package ru.nsu.ccfit.khudyakov.railway_station.utils;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import ru.nsu.ccfit.khudyakov.railway_station.employees.model.Cashier;
import ru.nsu.ccfit.khudyakov.railway_station.employees.model.EmployeeTO;


public class EmployeesParamsBuilder {
    /*
    *   Employees args
     */
    private final static String EMPLOYEE_ID_ARG = "employee_id";
    private final static String FIRST_NAME_ARG = "first_name";
    private final static String LAST_NAME_ARG = "last_name";
    private final static String MIDDLE_NAME_ARG = "middle_name";
    private final static String GENDER_ARG = "gender";
    private final static String AGE_ARG = "age";
    private final static String AMOUNT_CHILD_ARG = "amount_child";
    private final static String SALARY_ARG = "salary";
    private final static String BRIGADE_ID_ARG = "brigade_id";
    private final static String STANDING_ARG = "standing";
    private final static String LEVEL_ARG = "level";
    private final static String LEVEL_NAME_ARG = "level_name";

    /*
     *   Departments args
     */
    private final static String DEPARTMENT_ID_ARG = "department_id";
    private final static String DEPARTMENT_NAME_ARG = "department_name";

    private static MapSqlParameterSource buildEmployeeParams(EmployeeTO employeeTO) {
        return new MapSqlParameterSource()
                .addValue(FIRST_NAME_ARG, employeeTO.getFirstName())
                .addValue(LAST_NAME_ARG, employeeTO.getLastName())
                .addValue(MIDDLE_NAME_ARG, employeeTO.getMiddleName())
                .addValue(GENDER_ARG, employeeTO.getGender())
                .addValue(AGE_ARG, employeeTO.getAge())
                .addValue(AMOUNT_CHILD_ARG, employeeTO.getAmountChild())
                .addValue(SALARY_ARG, employeeTO.getSalary())
                .addValue(BRIGADE_ID_ARG, employeeTO.getBrigadeID())
                .addValue(STANDING_ARG, employeeTO.getStanding());
    }

    public static SqlParameterSource buildCashierParams(Cashier cashier) {
        MapSqlParameterSource parameterSource = buildEmployeeParams(cashier);
        parameterSource.addValue(LEVEL_ARG, cashier.getLevel());
        return parameterSource;
    }

    public static String getEmployeeIdArg() {
        return EMPLOYEE_ID_ARG;
    }

    public static String getFirstNameArg() {
        return FIRST_NAME_ARG;
    }

    public static String getLastNameArg() {
        return LAST_NAME_ARG;
    }

    public static String getMiddleNameArg() {
        return MIDDLE_NAME_ARG;
    }

    public static String getGenderArg() {
        return GENDER_ARG;
    }

    public static String getAgeArg() {
        return AGE_ARG;
    }

    public static String getAmountChildArg() {
        return AMOUNT_CHILD_ARG;
    }

    public static String getSalaryArg() {
        return SALARY_ARG;
    }

    public static String getBrigadeIdArg() {
        return BRIGADE_ID_ARG;
    }

    public static String getStandingArg() {
        return STANDING_ARG;
    }

    public static String getLevelArg() {
        return LEVEL_ARG;
    }

    public static String getLevelNameArg() {
        return LEVEL_NAME_ARG;
    }

    public static String getDepartmentIdArg() {
        return DEPARTMENT_ID_ARG;
    }

    public static String getDepartmentNameArg() {
        return DEPARTMENT_NAME_ARG;
    }
}
