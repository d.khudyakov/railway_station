package ru.nsu.ccfit.khudyakov.railway_station.employees.general;

import ru.nsu.ccfit.khudyakov.railway_station.employees.model.EmployeeID;
import ru.nsu.ccfit.khudyakov.railway_station.employees.model.EmployeeTO;

import java.util.List;
import java.util.Map;

public interface EmployeesDao {
    List<EmployeeTO> getEmployees();

    List<EmployeeTO> getEmployeesWithDepartment(int departmentID);

    List<EmployeeTO> getEmployeesWithSalary(int leftBorder, int rightBorder);

    List<EmployeeTO> getEmployeesWithBrigade(int brigadeID);

    List<EmployeeTO> getDepartmentsManagers();

    void updateEmployee(EmployeeTO employee);

    void deleteEmployee(EmployeeID employeeID);
}
