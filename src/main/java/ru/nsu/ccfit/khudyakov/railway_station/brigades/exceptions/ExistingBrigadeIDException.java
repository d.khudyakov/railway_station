package ru.nsu.ccfit.khudyakov.railway_station.brigades.exceptions;

import java.sql.SQLException;

public class ExistingBrigadeIDException extends SQLException {
    public ExistingBrigadeIDException(String message) {
        super(message);
    }
}
