package ru.nsu.ccfit.khudyakov.railway_station.employees.cashiers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import ru.nsu.ccfit.khudyakov.railway_station.employees.model.Cashier;
import ru.nsu.ccfit.khudyakov.railway_station.utils.EmployeesParamsBuilder;

import javax.sql.DataSource;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class CashiersDaoImpl implements CashiersDao {
    private static final String SQL_GET_CASHIERS_LEVEL = "SELECT * FROM \"CashiersLevels\"";

    private static final String SQL_GET_CASHIERS =
            "SELECT * FROM \"Employees\" " +
                    "INNER JOIN \"Cashiers\" USING(\"employee_id\") ";

    private static final String SQL_GET_BRIGADES =
            "SELECT \"brigade_id\" FROM \"Brigade\" " +
                    "WHERE \"department_id\" = 3";

    private static final String SQL_UPDATE_CASHIER =
            "UPDATE \"Cashiers\" SET" +
                    "\"level\" = :level " +
                    "where \"employee_id\" = :employee_id";

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final DataSource dataSource;

    @Autowired
    public CashiersDaoImpl(NamedParameterJdbcTemplate jdbcTemplate, DataSource dataSource) {
        this.jdbcTemplate = jdbcTemplate;
        this.dataSource = dataSource;
    }

    @Override
    public List<Cashier> getCashiers() {
        Map<Integer, String> cashiersLevels = getCashierLevels();

        return jdbcTemplate.query(
                SQL_GET_CASHIERS,
                new MapSqlParameterSource(),
                (resultSet, i) ->
                        new Cashier(
                                resultSet.getInt(EmployeesParamsBuilder.getEmployeeIdArg()),
                                resultSet.getString(EmployeesParamsBuilder.getFirstNameArg()),
                                resultSet.getString(EmployeesParamsBuilder.getLastNameArg()),
                                resultSet.getString(EmployeesParamsBuilder.getMiddleNameArg()),
                                resultSet.getString(EmployeesParamsBuilder.getGenderArg()),
                                resultSet.getInt(EmployeesParamsBuilder.getAgeArg()),
                                resultSet.getInt(EmployeesParamsBuilder.getAmountChildArg()),
                                resultSet.getInt(EmployeesParamsBuilder.getSalaryArg()),
                                resultSet.getInt(EmployeesParamsBuilder.getBrigadeIdArg()),
                                resultSet.getInt(EmployeesParamsBuilder.getStandingArg()),
                                cashiersLevels.get(resultSet.getInt(EmployeesParamsBuilder.getLevelArg()))
                        ));
    }

    @Override
    public List<Integer> getBrigadeIDList() {
        return jdbcTemplate.query(
                SQL_GET_BRIGADES,
                new MapSqlParameterSource(),
                (resultSet, i) -> resultSet.getInt(EmployeesParamsBuilder.getBrigadeIdArg()));
    }

    @Override
    public Map<Integer, String> getCashierLevels() {
        Map<Integer, String> cashiersLevels = new HashMap<>();

        jdbcTemplate.query(
                SQL_GET_CASHIERS_LEVEL,
                new MapSqlParameterSource(),
                (resultSet, i) -> cashiersLevels.put(
                        resultSet.getInt(EmployeesParamsBuilder.getLevelArg()),
                        resultSet.getString(EmployeesParamsBuilder.getLevelNameArg())
                ));

        return cashiersLevels;
    }

    @Override
    public void addNewCashier(Cashier cashier) {
        SqlParameterSource parameterSource = EmployeesParamsBuilder.buildCashierParams(cashier);

        SimpleJdbcCall SimpleJdbcCall = new SimpleJdbcCall(dataSource)
                .withoutProcedureColumnMetaDataAccess()
                .withProcedureName("ADD_CASHIER")
                .declareParameters(
                        new SqlParameter(EmployeesParamsBuilder.getFirstNameArg(), Types.VARCHAR),
                        new SqlParameter(EmployeesParamsBuilder.getLastNameArg(), Types.VARCHAR),
                        new SqlParameter(EmployeesParamsBuilder.getMiddleNameArg(), Types.VARCHAR),
                        new SqlParameter(EmployeesParamsBuilder.getGenderArg(), Types.VARCHAR),
                        new SqlParameter(EmployeesParamsBuilder.getAgeArg(), Types.INTEGER),
                        new SqlParameter(EmployeesParamsBuilder.getAmountChildArg(), Types.INTEGER),
                        new SqlParameter(EmployeesParamsBuilder.getSalaryArg(), Types.INTEGER),
                        new SqlParameter(EmployeesParamsBuilder.getBrigadeIdArg(), Types.INTEGER),
                        new SqlParameter(EmployeesParamsBuilder.getStandingArg(), Types.INTEGER),
                        new SqlParameter(EmployeesParamsBuilder.getLevelArg(), Types.INTEGER));

        SimpleJdbcCall.execute(parameterSource);
    }

    @Override
    public void updateCashier(Cashier cashier) {
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue(EmployeesParamsBuilder.getEmployeeIdArg(), cashier.getEmployeeID())
                .addValue(EmployeesParamsBuilder.getLevelArg(), cashier.getLevel());

        jdbcTemplate.update(SQL_UPDATE_CASHIER, parameterSource);
    }
}
