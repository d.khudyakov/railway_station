package ru.nsu.ccfit.khudyakov.railway_station.brigades;

import ru.nsu.ccfit.khudyakov.railway_station.brigades.exceptions.ExistingBrigadeIDException;
import ru.nsu.ccfit.khudyakov.railway_station.brigades.model.Brigade;
import ru.nsu.ccfit.khudyakov.railway_station.departments.model.Department;

import java.util.List;
import java.util.Map;

public interface BrigadesDao {
    List<Brigade> getBrigades();

    Map<Integer, String> getDepartmentNames();

    void addBrigade(Brigade brigade) throws ExistingBrigadeIDException;
}
