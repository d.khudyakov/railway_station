package ru.nsu.ccfit.khudyakov.railway_station.employees.cashiers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.nsu.ccfit.khudyakov.railway_station.employees.general.EmployeesService;
import ru.nsu.ccfit.khudyakov.railway_station.employees.model.*;

import java.util.Arrays;
import java.util.List;

@Controller
public class CashiersController {
    private static final List<String> genders = Arrays.asList("М", "Ж");

    private final CashiersService cashiersService;
    private final EmployeesService employeesService;

    @Autowired
    public CashiersController(CashiersService cashiersService, EmployeesService employeesService) {
        this.cashiersService = cashiersService;
        this.employeesService = employeesService;
    }

    @GetMapping("/cashiers")
    @ResponseStatus(HttpStatus.OK)
    public String getCashiers(Model model) {
        model.addAttribute("cashiers", cashiersService.getCashiers());
        return "/cashiers";
    }

    @GetMapping("/cashiers/action/add")
    public String addCashier(Model model) {
        model.addAttribute("brigadeIDList", cashiersService.getBrigadeIDList());
        model.addAttribute("levels", cashiersService.getCashierLevels());
        model.addAttribute("newCashier", new Cashier());
        model.addAttribute("genders", genders);

        return "/cashier_add";
    }

    @PostMapping("/cashiers/action/save")
    public String addNewCashier(Model model, @Validated(NewEmployee.class) @ModelAttribute(value = "newCashier") Cashier cashier, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("brigadeIDList", cashiersService.getBrigadeIDList());
            model.addAttribute("levels", cashiersService.getCashierLevels());
            model.addAttribute("newCashier", cashier);
            model.addAttribute("genders", genders);

            return "/cashier_add";
        } else {
            cashiersService.addNewCashier(cashier);
            return "redirect:/cashiers";
        }
    }

    @GetMapping("/cashiers/action/edit")
    public String editCashier(Model model, @ModelAttribute(value = "editedCashier") Cashier cashier) {
        model.addAttribute("brigadeIDList", cashiersService.getBrigadeIDList());
        model.addAttribute("levels", cashiersService.getCashierLevels());
        model.addAttribute("cashier", cashier);
        return "/cashier_edit";
    }

    @PostMapping("/cashiers/action/update")
    public String employeeUpdate(@Validated(UpdatedEmployee.class) @ModelAttribute(value = "cashier") Cashier cashier,
                                 BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "/cashier_edit";
        } else {
            employeesService.updateEmployee(cashier);
            cashiersService.updateCashier(cashier);
            return "redirect:/cashiers";
        }
    }

    @PostMapping("/cashiers/action/delete")
    public String deleteCashier(@ModelAttribute(value = "employeeID") EmployeeID employeeID) {
        employeesService.deleteEmployee(employeeID);
        return "redirect:/cashiers";
    }
}
