package ru.nsu.ccfit.khudyakov.railway_station.employees.cashiers;

import org.springframework.stereotype.Service;
import ru.nsu.ccfit.khudyakov.railway_station.employees.model.Cashier;

import java.util.List;
import java.util.Map;

@Service
public interface CashiersService {
    List<Cashier> getCashiers();

    List<Integer> getBrigadeIDList();

    Map<Integer, String> getCashierLevels();

    void addNewCashier(Cashier cashier);

    void updateCashier(Cashier cashier);
}
