package ru.nsu.ccfit.khudyakov.railway_station.employees.model;

import javax.persistence.Column;

public class Cashier extends EmployeeTO {
    @Column(name = "level")
    private Integer level;

    @Column(name = "levelName")
    private String levelName;

    public Cashier(Integer employeeID,
                   String firstName,
                   String lastName,
                   String middleName,
                   String gender,
                   Integer age,
                   Integer amountChild,
                   Integer salary,
                   Integer brigadeID,
                   Integer standing,
                   String levelName) {
        super(employeeID, firstName, lastName, middleName, gender, age, amountChild, salary, brigadeID, standing);
        this.levelName = levelName;

    }

    public Cashier(Integer employeeID,
                   String firstName,
                   String lastName,
                   String middleName,
                   String gender,
                   Integer age,
                   Integer amountChild,
                   Integer salary,
                   Integer brigadeID,
                   Integer standing,
                   Integer level) {
        super(employeeID, firstName, lastName, middleName, gender, age, amountChild, salary, brigadeID, standing);
        this.level = level;
    }
    public Cashier() {
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }
}
