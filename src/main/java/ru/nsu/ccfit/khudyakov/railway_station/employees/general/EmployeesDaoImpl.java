package ru.nsu.ccfit.khudyakov.railway_station.employees.general;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;
import ru.nsu.ccfit.khudyakov.railway_station.employees.model.EmployeeID;
import ru.nsu.ccfit.khudyakov.railway_station.employees.model.EmployeeTO;
import ru.nsu.ccfit.khudyakov.railway_station.utils.EmployeesParamsBuilder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class EmployeesDaoImpl implements EmployeesDao {

    private static final String SQL_GET_EMPLOYEES = "SELECT * FROM \"Employees\"";

    private static final String SQL_GET_EMPLOYEES_WITH_DEPARTMENT =
            "SELECT \"employee_id\", \"last_name\", \"first_name\", \"middle_name\"," +
                    "\"gender\", \"age\", \"amount_child\", \"salary\",\"brigade_id\", \"standing\" " +
                    "FROM \"Employees\" INNER JOIN \"Brigade\" USING(\"brigade_id\") " +
                    "INNER JOIN \"Departments\" USING (\"department_id\") " +
                    "WHERE \"department_id\" = :department_id";

    private static final String SQL_GET_EMPLOYEES_WITH_SALARY =
            "SELECT * FROM \"Employees\" WHERE \"salary\" >= :left_border AND\"salary\" <= :right_border";

    private static final String SQL_GET_EMPLOYEES_WITH_BRIGADE =
            "SELECT *  FROM \"Employees\" WHERE \"brigade_id\" = :brigade_id";

    private static final String SQL_UPDATE_EMPLOYEE =
            "UPDATE \"Employees\" SET" +
                    "\"age\" = :age ," +
                    "\"amount_child\" = :amount_child ," +
                    "\"salary\" = :salary ," +
                    "\"brigade_id\" = :brigade_id ," +
                    "\"standing\" = :standing " +
                    "where \"employee_id\" = :employee_id";

    private static final String SQL_DELETE_EMPLOYEE =
            "DELETE FROM \"Employees\" " +
                    "WHERE \"employee_id\" = :employee_id";

    private static final String SQL_GET_DEPARTMENTS_MANAGERS =
            "SELECT \"employee_id\", \"last_name\", \"first_name\", " +
                    "\"middle_name\", \"gender\", \"age\", \"amount_child\", \"salary\", \"brigade_id\", \"standing\" " +
                    "FROM \"Employees\" INNER JOIN \"Brigade\" USING(\"brigade_id\") " +
                    "RIGHT JOIN  \"Departments\" ON \"employee_id\" = \"manager_id\"";

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public EmployeesDaoImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private EmployeeTO buildEmployee(ResultSet resultSet) throws SQLException {
        return new EmployeeTO(
                resultSet.getInt(EmployeesParamsBuilder.getEmployeeIdArg()),
                resultSet.getString(EmployeesParamsBuilder.getLastNameArg()),
                resultSet.getString(EmployeesParamsBuilder.getLastNameArg()),
                resultSet.getString(EmployeesParamsBuilder.getMiddleNameArg()),
                resultSet.getString(EmployeesParamsBuilder.getGenderArg()),
                resultSet.getInt(EmployeesParamsBuilder.getAgeArg()),
                resultSet.getInt(EmployeesParamsBuilder.getAmountChildArg()),
                resultSet.getInt(EmployeesParamsBuilder.getSalaryArg()),
                resultSet.getInt(EmployeesParamsBuilder.getBrigadeIdArg()),
                resultSet.getInt(EmployeesParamsBuilder.getStandingArg())
        );
    }

    @Override
    public List<EmployeeTO> getEmployees() {
        return jdbcTemplate.query(
                SQL_GET_EMPLOYEES,
                new MapSqlParameterSource(),
                (resultSet, i) -> buildEmployee(resultSet));
    }

    @Override
    public List<EmployeeTO> getEmployeesWithDepartment(int departmentID) {
        return jdbcTemplate.query(
                SQL_GET_EMPLOYEES_WITH_DEPARTMENT,
                new MapSqlParameterSource().addValue(EmployeesParamsBuilder.getDepartmentIdArg(), departmentID),
                (resultSet, i) -> buildEmployee(resultSet));
    }

    @Override
    public List<EmployeeTO> getEmployeesWithSalary(int leftBorder, int rightBorder) {
        return jdbcTemplate.query(
                SQL_GET_EMPLOYEES_WITH_SALARY,
                new MapSqlParameterSource()
                        .addValue("left_border", leftBorder)
                        .addValue("right_border", rightBorder),
                (resultSet, i) -> buildEmployee(resultSet));
    }

    @Override
    public List<EmployeeTO> getEmployeesWithBrigade(int brigadeID) {
        return jdbcTemplate.query(
                SQL_GET_EMPLOYEES_WITH_BRIGADE,
                new MapSqlParameterSource().addValue(EmployeesParamsBuilder.getBrigadeIdArg(), brigadeID),
                (resultSet, i) -> buildEmployee(resultSet));
    }

    @Override
    public List<EmployeeTO> getDepartmentsManagers() {
        return jdbcTemplate.query(
                SQL_GET_DEPARTMENTS_MANAGERS,
                new MapSqlParameterSource(),
                (resultSet, i) -> buildEmployee(resultSet));
    }

    @Override
    public void updateEmployee(EmployeeTO employeeTO) {
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue(EmployeesParamsBuilder.getEmployeeIdArg(), employeeTO.getEmployeeID())
                .addValue(EmployeesParamsBuilder.getAgeArg(), employeeTO.getAge())
                .addValue(EmployeesParamsBuilder.getAmountChildArg(), employeeTO.getAmountChild())
                .addValue(EmployeesParamsBuilder.getSalaryArg(), employeeTO.getSalary())
                .addValue(EmployeesParamsBuilder.getBrigadeIdArg(), employeeTO.getBrigadeID())
                .addValue(EmployeesParamsBuilder.getStandingArg(), employeeTO.getStanding());

        jdbcTemplate.update(SQL_UPDATE_EMPLOYEE, parameterSource);
    }

    @Override
    public void deleteEmployee(EmployeeID employeeID) {
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue(EmployeesParamsBuilder.getEmployeeIdArg(), employeeID.getEmployeeID());

        jdbcTemplate.update(SQL_DELETE_EMPLOYEE, parameterSource);
    }


}
