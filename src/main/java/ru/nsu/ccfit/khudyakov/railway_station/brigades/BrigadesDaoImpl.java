package ru.nsu.ccfit.khudyakov.railway_station.brigades;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;
import ru.nsu.ccfit.khudyakov.railway_station.brigades.exceptions.ExistingBrigadeIDException;
import ru.nsu.ccfit.khudyakov.railway_station.brigades.model.Brigade;
import ru.nsu.ccfit.khudyakov.railway_station.departments.model.Department;
import ru.nsu.ccfit.khudyakov.railway_station.utils.EmployeesParamsBuilder;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class BrigadesDaoImpl implements BrigadesDao {

    private static final String SQL_GET_BRIGADES =
            "SELECT * FROM \"Brigade\"";

    private static final String SQL_ADD_BRIGADE =
            "INSERT INTO \"Brigade\" VALUES(:brigade_id, :department_id)";

    private static final String SQL_GET_DEPARTMENTS_NAMES =
            "SELECT \"department_id\",\"department_name\" FROM \"Departments\"";

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public BrigadesDaoImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Brigade> getBrigades() {
        Map<Integer, String> departmentNames = getDepartmentNames();

        return jdbcTemplate.query(
                SQL_GET_BRIGADES,
                new MapSqlParameterSource(),
                (resultSet, i) -> new Brigade(
                        resultSet.getInt(EmployeesParamsBuilder.getBrigadeIdArg()),
                        departmentNames.get(resultSet.getInt(EmployeesParamsBuilder.getDepartmentIdArg()))
                )
        );
    }

    @Override
    public Map<Integer, String> getDepartmentNames() {
        Map<Integer, String> departmentNames = new HashMap<>();

        jdbcTemplate.query(
                SQL_GET_DEPARTMENTS_NAMES,
                new MapSqlParameterSource(),
                (resultSet, i) ->
                        departmentNames.put(
                                resultSet.getInt(EmployeesParamsBuilder.getDepartmentIdArg()),
                                resultSet.getString(EmployeesParamsBuilder.getDepartmentNameArg())
                        )
        );

        return departmentNames;
    }

    @Override
    public void addBrigade(Brigade brigade) throws ExistingBrigadeIDException {
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue(EmployeesParamsBuilder.getBrigadeIdArg(), brigade.getBrigadeID())
                .addValue(EmployeesParamsBuilder.getDepartmentIdArg(), brigade.getDepartmentID());

        try {
            jdbcTemplate.update(
                    SQL_ADD_BRIGADE,
                    parameterSource
            );
        } catch (DataAccessException dataAccessException) {
            throw new ExistingBrigadeIDException("Данный номер бригады уже занят");
        }
    }
}
