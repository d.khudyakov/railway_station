package ru.nsu.ccfit.khudyakov.railway_station.employees.general;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.nsu.ccfit.khudyakov.railway_station.brigades.BrigadesService;
import ru.nsu.ccfit.khudyakov.railway_station.employees.model.EmployeeID;
import ru.nsu.ccfit.khudyakov.railway_station.employees.model.EmployeeTO;
import ru.nsu.ccfit.khudyakov.railway_station.employees.model.UpdatedEmployee;

@Controller
public class EmployeesController {
    private final EmployeesService employeesService;
    private final BrigadesService brigadesService;

    @Autowired
    public EmployeesController(EmployeesService employeesService, BrigadesService brigadesService) {
        this.employeesService = employeesService;
        this.brigadesService = brigadesService;
    }

    private void initModelParameters(Model model) {
        model.addAttribute("departments", brigadesService.getDepartmentNames());
        model.addAttribute("brigades", brigadesService.getBrigades());
    }

    @GetMapping({"/employees", "/", "/index"})
    public String getEmployees(Model model) {
        initModelParameters(model);
        model.addAttribute("employees", employeesService.getEmployees());
        return "/employees";
    }

    @GetMapping("/employees/sort/department")
    public String getEmployeesWithDepartment(Model model, Integer department) {
        initModelParameters(model);
        model.addAttribute("employees", employeesService.getEmployeesWithDepartment(department));
        return "/employees";
    }

    @GetMapping("/employees/sort/salary")
    public String getEmployeesWithSalary(Model model, Integer leftBorder, Integer rightBorder) {
        initModelParameters(model);

        if (leftBorder == null || rightBorder == null) {
            model.addAttribute("employees", employeesService.getEmployees());
            model.addAttribute("error", "Границы должны быть заполнены");
        } else {
            model.addAttribute("employees", employeesService.getEmployeesWithSalary(leftBorder, rightBorder));
        }
        return "/employees";
    }

    @GetMapping("/employees/sort/brigade")
    public String getEmployeesWithBrigade(Model model, Integer brigadeID) {
        initModelParameters(model);
        model.addAttribute("employees", employeesService.getEmployeesWithBrigade(brigadeID));
        return "/employees";
    }

    @GetMapping("/employees/sort/managers")
    public String getEmployeesWithDepartment(Model model) {
        initModelParameters(model);
        model.addAttribute("employees", employeesService.getDepartmentsManagers());
        return "/employees";
    }

    @GetMapping("/employees/action/edit")
    public String employeeEdit(Model model, @ModelAttribute(value = "editedEmployee") EmployeeTO employee) {
        model.addAttribute("employee", employee);
        return "/employee_edit";
    }

    @PostMapping("/employees/action/update")
    public String employeeUpdate(@Validated(UpdatedEmployee.class) @ModelAttribute(value = "employee") EmployeeTO employee,
                                 BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "employee_edit";
        } else {
            employeesService.updateEmployee(employee);
            return "redirect:/employees";
        }
    }

    @PostMapping("/employees/action/delete")
    public String employeeDelete(@ModelAttribute(value = "employeeID") EmployeeID employeeID) {
        employeesService.deleteEmployee(employeeID);
        return "redirect:/employees";
    }

}
