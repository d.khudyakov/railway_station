package ru.nsu.ccfit.khudyakov.railway_station.employees.general;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.nsu.ccfit.khudyakov.railway_station.employees.model.EmployeeID;
import ru.nsu.ccfit.khudyakov.railway_station.employees.model.EmployeeTO;

import java.util.List;
import java.util.Map;

@Component
public class EmployeesServiceImpl implements EmployeesService {

    private final EmployeesDao employeesDao;

    @Autowired
    public EmployeesServiceImpl(EmployeesDao employeesDao) {
        this.employeesDao = employeesDao;
    }

    @Override
    public List<EmployeeTO> getEmployees() {
        return employeesDao.getEmployees();
    }

    @Override
    public List<EmployeeTO> getEmployeesWithDepartment(int departmentID) {
        return employeesDao.getEmployeesWithDepartment(departmentID);
    }

    @Override
    public List<EmployeeTO> getEmployeesWithSalary(int leftBorder, int rightBorder) {
        return employeesDao.getEmployeesWithSalary(leftBorder, rightBorder);
    }

    @Override
    public List<EmployeeTO> getEmployeesWithBrigade(int brigadeID) {
        return employeesDao.getEmployeesWithBrigade(brigadeID);
    }

    @Override
    public void updateEmployee(EmployeeTO employee) {
        employeesDao.updateEmployee(employee);
    }

    @Override
    public void deleteEmployee(EmployeeID employeeID) {
        employeesDao.deleteEmployee(employeeID);
    }

    @Override
    public List<EmployeeTO> getDepartmentsManagers() {
        return employeesDao.getDepartmentsManagers();
    }

}
