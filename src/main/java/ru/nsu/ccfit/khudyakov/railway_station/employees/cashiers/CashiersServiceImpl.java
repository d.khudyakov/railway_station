package ru.nsu.ccfit.khudyakov.railway_station.employees.cashiers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.nsu.ccfit.khudyakov.railway_station.employees.model.Cashier;

import java.util.List;
import java.util.Map;

@Component
public class CashiersServiceImpl implements CashiersService {

    private final CashiersDao cashiersDao;

    @Autowired
    public CashiersServiceImpl(CashiersDao cashiersDao) {
        this.cashiersDao = cashiersDao;
    }

    @Override
    public List<Cashier> getCashiers() {
        return cashiersDao.getCashiers();
    }

    @Override
    public List<Integer> getBrigadeIDList() {
        return cashiersDao.getBrigadeIDList();
    }

    @Override
    public Map<Integer, String> getCashierLevels() {
        return cashiersDao.getCashierLevels();
    }

    @Override
    public void addNewCashier(Cashier cashier) {
        cashiersDao.addNewCashier(cashier);
    }

    @Override
    public void updateCashier(Cashier cashier) {
        cashiersDao.updateCashier(cashier);
    }
}
