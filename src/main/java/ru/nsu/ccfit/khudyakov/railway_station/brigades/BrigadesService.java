package ru.nsu.ccfit.khudyakov.railway_station.brigades;

import org.springframework.stereotype.Service;
import ru.nsu.ccfit.khudyakov.railway_station.brigades.exceptions.ExistingBrigadeIDException;
import ru.nsu.ccfit.khudyakov.railway_station.brigades.model.Brigade;

import java.util.List;
import java.util.Map;

@Service
public interface BrigadesService {
    List<Brigade> getBrigades();

    Map<Integer, String> getDepartmentNames();

    void addBrigade(Brigade brigade) throws ExistingBrigadeIDException;
}
