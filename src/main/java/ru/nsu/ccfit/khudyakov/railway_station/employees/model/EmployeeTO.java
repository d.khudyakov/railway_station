package ru.nsu.ccfit.khudyakov.railway_station.employees.model;

import javax.persistence.Column;
import javax.validation.constraints.*;

public class EmployeeTO {
    @Null(groups = NewEmployee.class)
    @Column(name = "employeeID")
    private Integer employeeID;

    @NotEmpty(groups = NewEmployee.class, message = "Не заполнено")
    @Column(name = "firstName")
    private String firstName;

    @NotEmpty(groups = NewEmployee.class, message = "Не заполнено")
    @Column(name = "lastName")
    private String lastName;

    @NotEmpty(groups = NewEmployee.class, message = "Не заполнено")
    @Column(name = "middleName")
    private String middleName;

    @NotNull(groups = NewEmployee.class, message = "Не заполнено")
    @Column(name = "brigadeID")
    private Integer brigadeID;

    @NotEmpty(groups = NewEmployee.class, message = "Не заполнено")
    @Column(name = "gender")
    private String gender;

    @NotNull(groups = {NewEmployee.class, UpdatedEmployee.class}, message = "Не заполнено")
    @PositiveOrZero(groups = {NewEmployee.class, UpdatedEmployee.class}, message = "Значение должно быть >= 0")
    @Column(name = "age")
    private Integer age;

    @NotNull(groups = {NewEmployee.class, UpdatedEmployee.class}, message = "Не заполнено")
    @PositiveOrZero(groups = {NewEmployee.class, UpdatedEmployee.class}, message = "Значение должно быть >= 0")
    @Column(name = "amountChild")
    private Integer amountChild;

    @NotNull(groups = {NewEmployee.class, UpdatedEmployee.class}, message = "Не заполнено")
    @PositiveOrZero(groups = {NewEmployee.class, UpdatedEmployee.class}, message = "Значение должно быть >= 0")
    @Column(name = "salary")
    private Integer salary;


    @NotNull(groups = {NewEmployee.class, UpdatedEmployee.class}, message = "Не заполнено       ")
    @PositiveOrZero(groups = {NewEmployee.class, UpdatedEmployee.class}, message = "Значение должно быть >= 0")
    @Column(name = "standing")
    private Integer standing;

    public EmployeeTO() {
    }

    public EmployeeTO(Integer employeeID,
                      String firstName,
                      String lastName,
                      String middleName,
                      String gender,
                      Integer age,
                      Integer amountChild,
                      Integer salary,
                      Integer brigadeID,
                      Integer standing) {

        this.employeeID = employeeID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.gender = gender;
        this.age = age;
        this.amountChild = amountChild;
        this.salary = salary;
        this.brigadeID = brigadeID;
        this.standing = standing;
    }

    public Integer getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(Integer employeeID) {
        this.employeeID = employeeID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getAmountChild() {
        return amountChild;
    }

    public void setAmountChild(Integer amountChild) {
        this.amountChild = amountChild;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public Integer getBrigadeID() {
        return brigadeID;
    }

    public void setBrigadeID(Integer brigadeID) {
        this.brigadeID = brigadeID;
    }

    public Integer getStanding() {
        return standing;
    }

    public void setStanding(Integer standing) {
        this.standing = standing;
    }
}
