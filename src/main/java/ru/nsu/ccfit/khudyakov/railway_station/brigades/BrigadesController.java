package ru.nsu.ccfit.khudyakov.railway_station.brigades;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.nsu.ccfit.khudyakov.railway_station.brigades.exceptions.ExistingBrigadeIDException;
import ru.nsu.ccfit.khudyakov.railway_station.brigades.model.Brigade;

import javax.validation.Valid;

@Controller
public class BrigadesController {

    private final BrigadesService brigadesService;

    @Autowired
    public BrigadesController(BrigadesService brigadesService) {
        this.brigadesService = brigadesService;
    }

    @GetMapping("/brigades")
    public String getBrigades(Model model) {
        model.addAttribute("brigades", brigadesService.getBrigades());
        model.addAttribute("newBrigade", new Brigade());
        model.addAttribute("departments", brigadesService.getDepartmentNames());
        return "brigades";
    }

    @PostMapping("/brigades/action/save")
    public String addBrigade(Model model,
                             @Valid @ModelAttribute(value = "newBrigade") Brigade brigade,
                             BindingResult bindingResult) {

        model.addAttribute("brigades", brigadesService.getBrigades());
        model.addAttribute("departments", brigadesService.getDepartmentNames());

        if (bindingResult.hasErrors()) {
            model.addAttribute("newBrigade", brigade);
        } else {
            try {
                brigadesService.addBrigade(brigade);
                model.addAttribute("newBrigade", new Brigade());
                return "redirect:/brigades";
            } catch (ExistingBrigadeIDException e) {
                model.addAttribute("newBrigade", brigade);
                model.addAttribute("error", e.getMessage());
            }
        }

        return "brigades";
    }
}
