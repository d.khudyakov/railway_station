package ru.nsu.ccfit.khudyakov.railway_station.brigades;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.nsu.ccfit.khudyakov.railway_station.brigades.exceptions.ExistingBrigadeIDException;
import ru.nsu.ccfit.khudyakov.railway_station.brigades.model.Brigade;

import java.util.List;
import java.util.Map;

@Component
public class BrigadesServiceImpl implements BrigadesService {

    private final BrigadesDao brigadesDao;

    @Autowired
    public BrigadesServiceImpl(BrigadesDao brigadesDao) {
        this.brigadesDao = brigadesDao;
    }

    @Override
    public List<Brigade> getBrigades() {
        return brigadesDao.getBrigades();
    }

    @Override
    public Map<Integer, String> getDepartmentNames() {
        return brigadesDao.getDepartmentNames();
    }

    @Override
    public void addBrigade(Brigade brigade) throws ExistingBrigadeIDException {
        brigadesDao.addBrigade(brigade);
    }
}
