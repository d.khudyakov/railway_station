package ru.nsu.ccfit.khudyakov.railway_station.brigades.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

public class Brigade {
    @NotNull(message = "Поле номер должно быть задано")
    @PositiveOrZero(message = "Поле номер должно быть больше или равно 0")
    private Integer brigadeID;

    @NotNull
    private Integer departmentID;

    private String departmentName;

    public Brigade() {
    }

    public Brigade(Integer brigadeID, String departmentName) {
        this.brigadeID = brigadeID;
        this.departmentName = departmentName;
    }

    public Integer getBrigadeID() {
        return brigadeID;
    }

    public void setBrigadeID(Integer brigadeID) {
        this.brigadeID = brigadeID;
    }

    public Integer getDepartmentID() {
        return departmentID;
    }

    public void setDepartmentID(Integer departmentID) {
        this.departmentID = departmentID;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
}
